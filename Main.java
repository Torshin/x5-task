import java.util.ArrayList;

public class Main {

  public static void main(String[] args) {
    Service service1 = new Service("1", new ArrayList<>(), 5);
    Service service2 = new Service("2", new ArrayList<>(), 2);
    Service service3 = new Service("3", new ArrayList<>(), 3);
    Service service4 = new Service("4", new ArrayList<>(), 4);
    Service service5 = new Service("5", new ArrayList<>(), 6);
    Service service6 = new Service("6", new ArrayList<>(), 8);
    service1.getChildren().add(service3);
    service2.getChildren().add(service3);
    service3.getChildren().add(service4);
    service4.getChildren().add(service5);
    service4.getChildren().add(service6);

    ArrayList<Service> services = new ArrayList<>();
    services.add(service1);
    services.add(service2);
    services.add(service3);
    services.add(service4);
    services.add(service5);
    services.add(service6);
    new ServiceExecutor(4).runServices(services);
  }
}
