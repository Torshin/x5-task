
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServiceExecutor {

  private final ExecutorService pool;
  private final List<Service> done = new ArrayList<>();


  public ServiceExecutor(int totalThreads) {
    pool = Executors.newFixedThreadPool(totalThreads);
  }

  public void runServices(List<Service> services) {
    checkCycles(services);

    int previousDoneSize;
    while (!services.isEmpty()) {
      previousDoneSize = done.size();
      Iterator<Service> iterator = services.iterator();
      while (iterator.hasNext()) {
        Service service = iterator.next();
        List<Service> children = service.getChildren();
        if (children != null) {
          children.removeAll(done);
        }
        if (children == null || children.isEmpty()) {
          pool.execute(getRunnableForService(service));
          iterator.remove();
        }
      }
      waitForServices(previousDoneSize);
    }
    pool.shutdown();
  }

  private Runnable getRunnableForService(Service service) {
    return () -> {
      service.execute();
      synchronized (done) {
        done.add(service);
        done.notify();
      }
    };
  }

  private void waitForServices(int previousDoneSize) {
    try {
      if (done.size() == previousDoneSize) {
        synchronized (done) {
          done.wait();
        }
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void checkCycles(List<Service> services) {
    for (Service service : services) {
      ArrayList<Service> children = new ArrayList<>();
      ArrayList<Service> parents = new ArrayList<>();
      parents.add(service);
      checkCycle(parents, service.getChildren());

    }
  }

  private void checkCycle(ArrayList<Service> parents, List<Service> children) {
    if (children != null) {
      if (children.stream().anyMatch(parents::contains)) {
        throw new IllegalArgumentException("Found cycles");
      }
      for (Service child : children) {
        ArrayList<Service> allParents = new ArrayList<>(parents);
        allParents.add(child);
        checkCycle(allParents, child.getChildren());
      }
    }
  }
}
