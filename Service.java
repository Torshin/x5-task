import java.util.List;

public class Service {

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Service> getChildren() {
    return children;
  }

  public void setChildren(List<Service> children) {
    this.children = children;
  }

  public int getSec() {
    return sec;
  }

  public void setSec(int sec) {
    this.sec = sec;
  }

  private String name;
  private List<Service> children;
  private int sec;

  public Service(String name, List<Service> children, int sec) {
    this.name = name;
    this.children = children;
    this.sec = sec;
  }

  public void execute() {
    System.out.println(name + " begin");
    try {
      Thread.sleep(sec * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(name + " end");
  }
}
